# ipset-sort

This utility implements a simple sort of the "ipset save" output for the Linux ipset utility, which as of version 6.29 does not implement the "-sorted" option.

# background

The Linux ipset utility implements a "save" sub-command, which outputs all current ipsets to `stdout`, which is then typically saved to a system 
configuration file (in RedHat, that's `/etc/sysconfig/ipset`). Unfortunately, elements in this array are not sorted, and because the source data structure
is a hash, the output is chaotically ordered. Thus, subsequent changes to a given ipset will result in several additions in deletions. When such a file
is maintained in git or other version-control system, a simple 'diff' will not easily show the differences between versions of the file, since these 
ipsets will have a different ordering.

The simple solution is to sort each list. The command supports a global option '-sorted' but as of 6.29, this version is not implemented. Thus, this
script exists to implement that particular feature.

# operation

Simply install somewhere (ie, `/usr/local/sbin`) and run `ipset-sort`. The program will use the current `$PATH` to launch the `ipset` utility with the `save` option, 
sort the resulting lists, and output to `stdout`.

# author

Otheus
2018-10-01